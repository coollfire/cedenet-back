package com.cidenet.round;

import com.cidenet.models.entity.Round;
import com.cidenet.models.entity.User;
import com.cidenet.repository.RoundRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@DataJpaTest
public class RoundTest {

    @Autowired
    RoundRepository roundRepository;

    @Test
    void ExistUserById() {
        Optional<Round> round = roundRepository.findById(1L);
        Assertions.assertTrue(round.isPresent());
    }

    @Test
    void FindAll() {
        List<Round> rounds = (List<Round>) roundRepository.findAll();
        Assertions.assertFalse(rounds.isEmpty());
        Assertions.assertEquals(2,rounds.size());

    }


}
