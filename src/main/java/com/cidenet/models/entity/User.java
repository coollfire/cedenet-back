package com.cidenet.models.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "user_db")
public class User  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long user_id;

    private String passw;

    private String name_one;

    private String name_others;

    private String last_name_one;

    private String last_name_two;

    private String country;

    private String identification;

    @Column(name = "id_num")
    private String numId;

    private String email;

    private Boolean status;

    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date create_at;

    @OneToMany(mappedBy = "user",cascade = CascadeType.ALL )
    private List<Round> rounds;




}
