package com.cidenet.controller;

import com.cidenet.models.entity.Round;
import com.cidenet.models.models.SingleDate;
import com.cidenet.models.models.SingleLong;
import com.cidenet.service.RoundService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/round")
@CrossOrigin(origins = "http://localhost:4200" )
public class RoundController {

    @Autowired
    private RoundService service;

    @ApiOperation("Crea una nuevo registro en la base de datos")
    @ApiResponse(code = 200,message = "Round entity")
    @PostMapping("/save")
    public ResponseEntity<?> save(@RequestBody Round round){

        Optional<Round>  roundSave = Optional.ofNullable(service.Save(round));

        if (roundSave.isEmpty()){
            return ResponseEntity.badRequest().body("No se crear la Ronda");
        }

        return ResponseEntity.ok(roundSave);

    }

    @ApiOperation("Elimina un registro por id o pk ")
    @ApiResponse(code = 200,message = "")
    @DeleteMapping("/delete")
    public ResponseEntity<?> delete(@RequestBody SingleLong id){

        service.delete(id.getId());

        return ResponseEntity.noContent().build();
    }
    @ApiOperation("Resgitra la entrada a un usuario, debe ingresar el id del resgitro")
    @ApiResponse(code = 200,message = "Fecha añadida")
    @PostMapping("/entry")
    public ResponseEntity<?> entry(@RequestBody SingleDate date){


        Optional<Round> round = Optional.ofNullable(service.entry(date.getDate(), date.getId()));

        if (round.isEmpty()){
            return ResponseEntity.badRequest().body("No se se pudo crear la entrada");
        }
        return ResponseEntity.ok("Fecha añadida");
    }

    @ApiOperation("Resgitra una salida a un usuario, debe ingresar el id del resgitro")
    @ApiResponse(code = 200,message = "Fecha añadida")
    @PostMapping("/exit")
    public ResponseEntity<?> exit(@RequestBody SingleDate date){

        Optional<Round> round = Optional.ofNullable(service.exit(date.getDate(), date.getId()));

        if (round.isEmpty()){
            return ResponseEntity.badRequest().body("No se se pudo crear la salida");
        }

        return ResponseEntity.ok("Fecha añadida");
    }

    @ApiOperation("Trae todos los registros de Round")
    @ApiResponse(code = 200,message = "No hay datos que consultar")
    @GetMapping("/all")
    public ResponseEntity<?> all(){

        List<Round> lista = service.findAll();

        if (lista.isEmpty()){
            return ResponseEntity.badRequest().body("No hay datos que consultar");
        }

        return ResponseEntity.ok(lista);
    }







}
