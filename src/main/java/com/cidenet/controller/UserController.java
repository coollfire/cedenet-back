package com.cidenet.controller;


import com.cidenet.models.entity.Round;
import com.cidenet.models.entity.User;
import com.cidenet.models.models.SingleData;
import com.cidenet.models.models.SingleLong;
import com.cidenet.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;


@RestController
@CrossOrigin(origins = "http://localhost:4200" )
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService service ;


    @ApiOperation("Guarda un nuevo usuario")
    @ApiResponse(code = 200,message = "User entity")
    @PostMapping("/save")
    public ResponseEntity<?> save( @RequestBody User user){


        System.out.println(user.getUser_id());
        return CreateUserPassEncry(user);
    }

    @ApiOperation("Modifica un usuario")
    @ApiResponse(code = 200,message = "User entity")
    @PutMapping("/save")
    public ResponseEntity<?> saveEdit( @RequestBody User user){

        return CreateUserPassEncry(user);
    }

    @ApiOperation("Elimina un  usuario")
    @ApiResponse(code = 200,message = "")
    @DeleteMapping("/delete")
    public ResponseEntity<?> delete(@RequestBody SingleLong id){
        service.delete(id.getId());

        return ResponseEntity.noContent().build();

    }
    @ApiOperation("Busqeda de un usuario por Email")
    @ApiResponse(code = 200,message = "User entity")
    @PostMapping("/email")
    public ResponseEntity<?> findEmail(@RequestBody SingleData data){
        Optional<User> email = service.findByEmail(data.getData());

        if(email.isPresent()){
            return ResponseEntity.ok(email.get());
        }
        return ResponseEntity.badRequest().body("No se encontro el Email: "+ data.getData());


    }

    @ApiOperation("Busqeda de un usuario por id o pk")
    @ApiResponse(code = 200,message = "User entity")
    @PostMapping("/find")
    public ResponseEntity<?> find(@RequestBody SingleLong id){


        Optional<User> user = service.FindByPk(id.getId());
        if (user.isPresent()){
            return ResponseEntity.ok(user.get());
        }
        return ResponseEntity.badRequest().body("No se encontro la llave: "+id.getId());

    }

    @ApiOperation("Busqeda de un usuario por identificacion")
    @ApiResponse(code = 200,message = "User entity")
    @GetMapping("/find/cc")
    public ResponseEntity<?> findbyid(@RequestBody SingleData data){
        Optional<User> user = service.findByNumId(data.getData());
        if (user.isPresent()){
            return ResponseEntity.ok(user.get());
        }
         return ResponseEntity.badRequest().body("No se encontro el docuemntos de identidad: "+ data.getData());
    }


    private ResponseEntity<?> CreateUserPassEncry(@RequestBody User user) {


        Optional<User> userSave = Optional.ofNullable(service.Save(user));

        if (userSave.isEmpty()){
            return ResponseEntity.badRequest().body("No se pudo crear el usuario");
        }

        return ResponseEntity.ok(userSave.get());
    }


    @ApiOperation("Trae todos los registros de User")
    @ApiResponse(code = 200,message = "No hay datos que consultar")
    @GetMapping("/all")
    public ResponseEntity<?> all(){

        List<User> lista = service.findAll();

        if (lista.isEmpty()){
            return ResponseEntity.badRequest().body("No hay datos que consultar");
        }

        return ResponseEntity.ok(lista);
    }
}
