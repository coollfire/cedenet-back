package com.cidenet.repository;


import com.cidenet.models.entity.Round;
import org.springframework.data.repository.CrudRepository;

public interface RoundRepository extends CrudRepository<Round,Long> {
}
