package com.cidenet.repository;


import com.cidenet.models.entity.Area;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AreaRepository extends CrudRepository<Area,Long> {

    Optional<Area> findByNameArea(String name);
}
