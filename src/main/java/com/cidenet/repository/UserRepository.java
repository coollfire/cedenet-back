package com.cidenet.repository;


import com.cidenet.models.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User,Long> {

    Optional<User> findByEmail(String email);

    Optional<User>findByNumId (String id);
}
