package com.cidenet.service;


import com.cidenet.models.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    Optional<User> FindByPk(Long id);

    Optional<User>findByEmail(String email);

    Optional<User>findByNumId(String id);

    List<User> findAll();

    User Save(User user);

    void delete(Long id);
}
