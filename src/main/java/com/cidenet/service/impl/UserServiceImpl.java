package com.cidenet.service.impl;

import com.cidenet.models.entity.User;
import com.cidenet.repository.UserRepository;
import com.cidenet.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Date;
import java.util.List;
import java.util.Optional;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    /**
     * Busca un usuario por ID o PK
     */
    public Optional<User> FindByPk(Long id) {
        return repository.findById(id);
    }



    @Override
    /**
     * Busca un usuario por email
     */
    public Optional<User> findByEmail(String email) {
        return repository.findByEmail(email);
    }

    @Override
    /**
     * Busca un usuario por idenficacion
     */
    public Optional<User> findByNumId(String id) {
        return repository.findByNumId(id);
    }

    @Override
    public List<User> findAll() {

        List<User> lista = (List<User>) repository.findAll();
        if (lista.isEmpty()){
            return  null;
        }

        return lista;
    }

    @Override
    /**
     * Genera correo con un formato en especifico
     * Valida no exist el correo
     * creo  o modifica usuario
     */
    public User Save(User user) {
        String email;
        String emailFinal;
        String base;
        boolean exists = true;
        int num = 0;

        if (user.getCountry().equals("cop")){

            base = user.getName_one().toLowerCase().trim() + "." +
                    user.getLast_name_one().toLowerCase().trim() ;
            email = "@cidenet.com.co";
            emailFinal = base + email;

        }else{
            base = user.getName_one().toLowerCase().trim() + "." +
                    user.getLast_name_one().toLowerCase().trim() ;

            email = "@cidenet.com.us";
            emailFinal = base+email;
        }


        do{
            Optional<User> userExists = findByEmail(emailFinal);

            if (userExists.isPresent()){
                emailFinal = base +"."+num + email;
                num ++;
                exists = true;
            }else{
                user.setEmail(emailFinal);
                exists = false;
            }
        }while(exists == true);

        if (user.getCreate_at() == null || user.getCreate_at().toString().equals("")  || user.getCreate_at().toString().equals(" ")){
            user.setCreate_at(new Date());
        }

        user.setStatus(true);

        return repository.save(user);
    }

    @Override
    public void delete(Long id) {

        Optional<User> user = repository.findById(id);

        if (user.isPresent()){
            repository.deleteById(id);
        }

    }
}
